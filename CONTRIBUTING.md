# Contributing 

1. Explicitly license your code, ideally under Unlicense but any permissive license works
2. All code is meant to be POSIX, so don't send non-POSIX code. if you see non-POSIX it's a bug.
3. Expanding on from 2, man 1p and shellcheck are your friends as that's the standard I use
4. 100 is the limit, 80 is the ideal. Comments and similar should be 100 but for code, try to
stick to 80. I am not a zealot so if you are like a few chars over 100 it's fine but don't push it.
5. Test it, this will save us both headaches
6. Generally just try to follow the style, I'd like to think my code is pretty readable so just 
stick to the way I do it and it's all good.
