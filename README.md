# ini - a simple ini processor

ini is essenitally jq but for ini files

Unlike jq however, the syntax is easy to understand and readable. 

To get information simply do the following:

```bash
$ ini <file> <title> <variable>
```

It must be in that order. 

It removes the quotes and any spacing so the result should always be:

```ini
key=value
```

meaning that ini is super predictable and easy to work with.

Obviously this is still very new so expect a bit of instability and changes to the syntax.

## Limitations 

Some of these are bugs or design flaws, some of these will not be fixed.

- Nesting only works via \[section.subsection\] \(won't fix\)
- Sections are required when they aren't required in standard ini \(might fix? though at that point use grep\)
- Quotes are the only supported escape sequence \(TODO\)
- Only = is supported for delimeters, no = or whitespace \(maybe\)
- Only # is supported for comments, you cannot use ; \(maybe\)
- Probably more problems honestly

## Contributing
See CONTRIBUTING.md
